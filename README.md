# CIMS2

## Before start

### Gitlab repository

Currently only 1 repositoy in Gitlab(https://gitlab.com/dhcims2/cims2) is connected to the pipeline. If you want to deploy via pipeline, you will need to commit the source to https://gitlab.com/dhcims2/cims2. In such case, it may have the possibility to overwrite others' commit on the Gitlab. Please consult other teammate before you commit.

### Naming

The application is running in the same namespace in Openshift. If you want to have your own project, to avoid collision, please update your `deployment.yaml`, `serivce.yaml` and `route.yaml` as follows:

|File|Attribute|Naming|
|-|-|-|
|deployment.yaml|metadata.name|`cims2-test-<your_corpid>`|
|service.yaml|metadata.name|`cims2-test-<your_corpid>-service`|
|route.yaml|metadata.name|`cims2-test-<your_corpid>-route`|
||spec.host|`<your_corpid>.cldpaast71.server.ha.org.hk`|
||spec.to.name|`cims2-test-<your_corpid>-service`|

### Source

Please visit https://gitlab.com/chw502/docker-k8s-openshift-sample/tree/master/openshift if you want to view the original source.

## Getting start

1. Follow the guide in [Naming](#naming) to update the files.
1. Commit the project to gitlab https://gitlab.com/dhcims2/cims2.
2. Login HA CI/CD pipeline with your CORPId & password http://wcdchci03:8089/dashboard/#/home/dashboard
3. Select `Release Pipeline` on the left panel, then select `CIMS` > `CIMS2` > `Run`
    ![](./img/pipeline.PNG)
4. Click the arrow next to `Run` to view pipeline console message.
4. When deployment is success 
    ![](./img/pipeline-success.PNG)
4. Login to HA Private Cloud [Openshift](https://cldpaast71-asm.server.ha.org.hk:8443/oauth/authorize?client_id=openshift-web-console&response_type=code&state=eyJ0aGVuIjoiL3Byb2plY3QvY2ltcy1kaC1vZGMtZGV2LTEvYnJvd3NlL2RlcGxveW1lbnRzIiwibm9uY2UiOiIxNTYxNTM1Nzc2NDE3LTIwMzM2NTAzMTAxMTQ2MTU4NzgyMzY3NTI4NzIzNjIxNDI4MDI1OTIyNDI0MjY2MTE4MTQ0MDQyODE3NzExODM1NjM1MjExMzUwMjM3MjE5In0&redirect_uri=https%3A%2F%2Fcldpaast71-asm.server.ha.org.hk%3A8443%2Fconsole%2Foauth), select `Domain`, input your **CORPDEV** & password.
5. Select `cims-db-odc-dev-1` on the right panel.
![](./img/openshift-home.PNG)
6. You can check your deployed application
7. On the left panel, go to `Application` > `Routes` to find the DNS of your application.

### Deployemnt

Please refer to [Deployment](https://gitlab.com/chw502/docker-k8s-openshift-sample/tree/master/kubernetes#deployment) in Kubernetes.

### Serivce

Please refer to [Service](https://gitlab.com/chw502/docker-k8s-openshift-sample/tree/master/kubernetes#service) in Kubernetes.

### Route

An OpenShift Container Platform route exposes a service at a host name, such as www.example.com, so that external clients can reach it by name. `route.yaml` provides corresponding configuration.


Ref:
- [Interactive Openshift tutorial](https://www.katacoda.com/openshift/courses/middleware)